#!/bin/bash

if [ -f ./tools/options.sh ] ; then
    source ./tools/options.sh
fi

./assets.sh
result=$?
if [ "$result" != 0 ]; then
    echo "error in assets"
    exit
fi

./buildcpp.sh
if [ "$result" != 0 ]; then
    echo "error in cpp build"
    exit
fi

./buildjavafix.sh
if [ "$result" != 0 ]; then
    echo "error in java fix build"
    exit
fi

./buildjava.sh
if [ "$result" != 0 ]; then
    echo "error in java build"
    exit
fi

./tools/sign.sh
