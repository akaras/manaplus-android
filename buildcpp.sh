#!/bin/bash

if [ -f ./tools/options.sh ] ; then
    source ./tools/options.sh
fi

DIR=`pwd`

cd android-project
ndk-build clean
# NDK_APPLICATION_MK=
ndk-build V=1 NDK_DEBUG=0 -j9 2>$DIR/logs/err2.log|tee $DIR/logs/err1.log

rm libs/*/gdb*
