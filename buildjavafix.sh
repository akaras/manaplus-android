#!/bin/bash

if [ -f ./tools/options.sh ] ; then
    source ./tools/options.sh
fi

DIR=`pwd`

sed -i -e "s/org.evolonline.beta.manaplus/org.libsdl.app/" android-project/AndroidManifest.xml
android update project -p android-project
cd android-project
ant release | tee $DIR/logs/ant.log
sed -i -e "s/org.libsdl.app/org.evolonline.beta.manaplus/" AndroidManifest.xml
