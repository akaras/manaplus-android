#!/bin/bash

export DIR=$(pwd)/extern
export PATH=${DIR}/ndk:${DIR}/sdk/tools:${DIR}/sdk/platform-tools:${DIR}/sdk/build-tools/23.0.1:${DIR}/sdk/build-tools/21.1.2:$PATH
