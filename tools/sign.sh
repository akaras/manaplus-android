#!/bin/bash

rm android-project/bin/ManaPlus.apk

if [ -f "tools/manaplus-release-key.keystore" ]; then
    jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore tools/manaplus-release-key.keystore \
    android-project/bin/ManaPlus-release-unsigned.apk manaplus
fi

zipalign -v 4 android-project/bin/ManaPlus-release-unsigned.apk android-project/bin/ManaPlus.apk
