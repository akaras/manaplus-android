#!/bin/bash

if [ -f ./tools/options.sh ] ; then
    source ./tools/options.sh
fi

DIR=`pwd`

android update project -p android-project
cd android-project
ant release | tee $DIR/logs/ant.log
