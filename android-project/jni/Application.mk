
# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
#APP_STL := gnustl_shared
APP_STL := gnustl_static
#APP_ABI := all
#APP_ABI := armeabi armeabi-v7a arm64-v8a mips64 mips x86 x86_64
# failed build arm64-v8a mips64 x86_64
APP_ABI := armeabi armeabi-v7a mips x86
NDK_TOOLCHAIN_VERSION=4.9
APP_CFLAGS := -O5
APP_OPTIM := release
