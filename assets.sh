#!/bin/sh

mkdir src/temp
cd src/temp

DIR=`pwd`

rm -f manaplus-data.zip*
rm -f manaplus-locale.zip
mkdir ../../android-project/assets
rm -rf ../../android-project/assets/manaplus-data.zip*
rm -rf ../../android-project/assets/manaplus-locale.zip

rm -rf data
mkdir data
cp -r ../data/* data
rm -rf data/evol
rm -rf data/tmw
zip "$DIR/manaplus-data.zip" -r data -x "*Makefile*" -x "*CMake*" -x "*.cmake" -x "*.in"

split -b 1000000 -d manaplus-data.zip manaplus-data.zip
rm -f manaplus-data.zip
cp manaplus-data.zip* ../../android-project/assets


cd ..
zip "$DIR/manaplus-locale.zip" -r locale -x "*Makefile*" -x "*CMake*" -x "*.cmake" -x "*.in"

cd $DIR
cp manaplus-locale.zip ../../android-project/assets
