#!/bin/bash

if [ ! -d "extern/sdk/tools" ]; then
    echo "Android SDK directory not exists: extern/sdk"
fi

if [ ! -d "extern/ndk/platforms" ]; then
    echo "Android NDK directory not exists: extern/ndk"
fi

if [ ! -d "src/manaplus" ]; then
    echo "ManaPlus sources not exists: src/manaplus"
fi
if [ ! -f "src/manaplus/Android.mk" ]; then
    echo "ManaPlus sources not exists: src/manaplus"
fi

if [ ! -d "android-project/jni/SDL/VisualC" ]; then
    echo "SDL2 sources not exists: android-project/jni/SDL"
fi

